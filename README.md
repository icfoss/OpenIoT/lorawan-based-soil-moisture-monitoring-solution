**LoRaWan Soil Moisture Node**

The proposed project  aims  at determining the most efficient  soil moisture monitoring method   in   an   long   range,   LoRa   Wireless   Communication   technology(LoraWAN).LoRaWAN Soil Moisture Sensor makes use of Analog Capacitive Soil Moisture by DFRobot and  ULPLoRaV2.2 getting it's data from LoraServer. It's Moisture Range is 0-100%. 

**Getting Started**

- Make sure that you have a ULP LoRa Board.

- Install project library from here . Copy the library to ~/Arduino/libraries/

- Select board : Arduino Pro Mini 3.3v 8Mhz

**Prerequisites**

Arduino IDE - 1.8.9 [Tested]

**License**
This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/SruthyS/lorawan_soil_moisture/-/blob/master/LICENSE) file for details

**Acknowledgments**

LMIC by [matthijskooijman](https://github.com/matthijskooijman/arduino-lmic)
